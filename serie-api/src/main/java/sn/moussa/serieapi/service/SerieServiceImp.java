package sn.moussa.serieapi.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sn.moussa.serieapi.dto.SerieResponse;

import java.util.Arrays;

@Service
public class SerieServiceImp implements SerieService {

    @Value("${server.port}")
    private String port;

    @Override
    public ResponseEntity<SerieResponse> getAllSeries() {
        return ResponseEntity.ok(SerieResponse
                .builder()
                .series(Arrays.asList(
                        "The Big Bang Theory",
                        "Hitman",
                        "The Witchers",
                        "Atypical"
                ))
                .port(port)
                .build());
    }
}
