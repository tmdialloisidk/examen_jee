package sn.moussa.serieapi.service;

import org.springframework.http.ResponseEntity;
import sn.moussa.serieapi.dto.SerieResponse;

public interface SerieService {
    ResponseEntity<SerieResponse> getAllSeries();
}
