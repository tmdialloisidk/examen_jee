package sn.moussa.serieapi.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class SerieResponse {
    private List<String> series;
    private String port;
}
