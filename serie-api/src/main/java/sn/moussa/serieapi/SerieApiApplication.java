package sn.moussa.serieapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class SerieApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SerieApiApplication.class, args);
	}

}
