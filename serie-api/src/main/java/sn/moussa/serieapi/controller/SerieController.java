package sn.moussa.serieapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.moussa.serieapi.dto.SerieResponse;
import sn.moussa.serieapi.service.SerieService;

@RestController
public class SerieController {
    private final SerieService serieService;

    public SerieController(SerieService serieService){
        this.serieService = serieService;
    }


    @GetMapping("/serie")
    ResponseEntity<SerieResponse> getSeries(){
        return serieService.getAllSeries();
    }
}
