package sn.moussa.filmapi.service;

import org.springframework.http.ResponseEntity;
import sn.moussa.filmapi.dto.FilmResponse;


public interface FilmService {
    ResponseEntity<FilmResponse> getAllFilms();
}
