package sn.moussa.filmapi.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import sn.moussa.filmapi.dto.FilmResponse;

import java.util.Arrays;

@Service
public class FilmServiceImp implements FilmService {
    @Value("${server.port}")
    private String port;

    @Override
    public ResponseEntity<FilmResponse> getAllFilms() {
        return ResponseEntity.ok(FilmResponse
                .builder()
                .films(Arrays.asList(
                        "Titanic",
                        "Odysse",
                        "Hunters"
                ))
                .port(port)
                .build());
    }
}
