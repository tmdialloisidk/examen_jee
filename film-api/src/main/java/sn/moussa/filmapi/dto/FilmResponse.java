package sn.moussa.filmapi.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@Builder
public class FilmResponse {
    private List<String> films;
    private String port;
}
