package sn.moussa.filmapi.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import sn.moussa.filmapi.dto.FilmResponse;
import sn.moussa.filmapi.service.FilmService;

@RestController
public class FilmController {

    private final FilmService filmService;

    public FilmController(FilmService filmService) {
        this.filmService = filmService;
    }

    @GetMapping("/films")
    ResponseEntity<FilmResponse> getFilms(){
        return filmService.getAllFilms();
    }
}
